import Web3ProviderEngine from 'web3-provider-engine';
const RpcSubprovider = require('web3-provider-engine/subproviders/rpc.js');
const FiltersSubprovider = require('web3-provider-engine/subproviders/filters.js');
const NonceSubprovider = require('web3-provider-engine/subproviders/nonce-tracker.js');
const TransportHID = require('@ledgerhq/hw-transport-node-hid').default;
import { createLedgerSubprovider } from './ledger-web3-subprovider';

export class LedgerProvider {
  private engine: Web3ProviderEngine;
  constructor({
    rpcUrl,
    ...options
  }: {
    networkId: number;
    rpcUrl: string;
    paths: string[];
    askConfirm: boolean;
    accountsLength: number;
    accountsOffset: number;
  }) {
    this.engine = new Web3ProviderEngine({});
    this.engine.addProvider(new FiltersSubprovider());
    this.engine.addProvider(new NonceSubprovider({}));
    const getTransport = () => TransportHID.create();
    const ledger = createLedgerSubprovider(getTransport, options);
    this.engine.addProvider(ledger);
    this.engine.addProvider(new RpcSubprovider({ rpcUrl }));
    this.engine.start();
  }

  sendAsync() {
    this.engine.sendAsync.apply(this.engine, arguments);
  }

  send() {
    throw new Error('Web3ProviderEngine does not support synchronous requests.');
  }

  stop() {
    this.engine.stop();
  }
}
