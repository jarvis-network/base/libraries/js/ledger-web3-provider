declare module 'web3-provider-engine' {
  export interface JSONRPCRequestPayload {
    params: any[];
    method: string;
    id: number;
    jsonrpc: string;
  }

  export interface JSONRPCResponsePayload {
    result: any;
    id: number;
    jsonrpc: string;
  }

  interface Web3ProviderEngineOptions {
    pollingInterval?: number;
    blockTracker?: any;
    blockTrackerProvider?: any;
  }

  export default class Web3ProviderEngine {
    constructor(options?: Web3ProviderEngineOptions);
    addProvider(provider: any): void;
    start(callback?: () => void): void;
    stop(): void;
    on(event: string, handler: () => void): void;
    send(payload: JSONRPCRequestPayload): void;
    sendAsync(
      payload: JSONRPCRequestPayload,
      callback: (error: null | Error, response: JSONRPCResponsePayload) => void,
    ): void;
  }
}
