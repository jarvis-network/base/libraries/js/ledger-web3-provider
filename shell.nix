{ pkgs ? import <nixpkgs> { } }:
with pkgs;
mkShell {
  buildInputs = [
    git
    nodejs-14_x
    (yarn.override { nodejs = nodejs-14_x; })
    python3
    gccStdenv
    eudev
    libusb1.dev
    pkg-config
    which
  ];
}
